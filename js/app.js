// Importar las funciones necesarias de los SDKs
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.1/firebase-app.js";
import { getDatabase, onValue, ref as refS, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/10.12.1/firebase-database.js";

// Configuración de Firebase
const firebaseConfig = {
  apiKey: "AIzaSyB8-PUMPVfUaERJe4o_34HKLWIMOMDA6-E",
  authDomain: "webcrud-4f9dc.firebaseapp.com",
  databaseURL: "https://webcrud-4f9dc-default-rtdb.firebaseio.com",
  projectId: "webcrud-4f9dc",
  storageBucket: "webcrud-4f9dc.appspot.com",
  messagingSenderId: "97055822393",
  appId: "1:97055822393:web:ac900ba73dad862da933e8"
};

// Inicializar Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);

// Declarar variables globales
var numSerie = 0;
var marca = "";
var modelo = "";
var descripcion = "";
var urlImag = "";

// Funciones
function leerInputs() {
  numSerie = document.getElementById('txtNumSerie').value;
  marca = document.getElementById('txtMarca').value;
  modelo = document.getElementById('txtModelo').value;
  descripcion = document.getElementById('txtDescripcion').value;
  urlImag = document.getElementById('txtUrl').value;
}

function mostrarMensaje(mensaje) {
  var mensajeElement = document.getElementById('mensaje');
  mensajeElement.textContent = mensaje;
  mensajeElement.style.display = 'block';
  setTimeout(() => {
    mensajeElement.style.display = 'none'
  }, 1000);
}

// Función para insertar un producto en la base de datos
function insertarProducto() {
  leerInputs(); // Asegúrate de que se llama correctamente a leerInputs

  // Validar los datos ingresados
  if (numSerie === "" || marca === "" || modelo === "" || descripcion === "") {
    mostrarMensaje("Faltaron datos por capturar");
    return;
  }

  // Guardar los datos en la base de datos
  set(
    refS(db, 'Automoviles/' + numSerie),
    {
      numSerie: numSerie,
      marca: marca,
      modelo: modelo,
      descripcion: descripcion,
      urlImag: urlImag
    }
  ).then(() => {
    alert("Se agregó con éxito");
    limpiarInputs();
    Listarproductos();
  }).catch((error) => {
    alert("Ocurrió un error: " + error);
  });
}

// Función para listar los productos
function Listarproductos() {
  const dbRef = refS(db, 'Automoviles');
  const tabla = document.getElementById('tablaProductos');
  const tbody = tabla.querySelector('tbody');
  tbody.innerHTML = '';
  onValue(dbRef, (snapshot) => {
    snapshot.forEach((childSnapshot) => {
      const childKey = childSnapshot.key;
      const data = childSnapshot.val();
      var fila = document.createElement('tr');
      var celdaCodigo = document.createElement('td');
      celdaCodigo.textContent = childKey;
      fila.appendChild(celdaCodigo);
      var celdaNombre = document.createElement('td');
      celdaNombre.textContent = data.marca;
      fila.appendChild(celdaNombre);
      var celdaPrecio = document.createElement('td');
      celdaPrecio.textContent = data.modelo;
      fila.appendChild(celdaPrecio);
      var celdaCantidad = document.createElement('td');
      celdaCantidad.textContent = data.descripcion;
      fila.appendChild(celdaCantidad);
      var celdaImagen = document.createElement('td');
      var imagen = document.createElement('img');
      imagen.src = data.urlImag;
      imagen.width = 100;
      celdaImagen.appendChild(imagen);
      fila.appendChild(celdaImagen);
      tbody.appendChild(fila);
    });
  });
}

function buscarAutomovile() {
  numSerie = document.getElementById('txtNumSerie').value.trim();
  if (numSerie === "") {
    mostrarMensaje("Faltó capturar el número de serie");
    return;
  }
  const dbref = refS(db);
  get(child(dbref, 'Automoviles/' + numSerie)).then((snapshot) => {
    if (snapshot.exists()) {
      marca = snapshot.val().marca;
      modelo = snapshot.val().modelo;
      descripcion = snapshot.val().descripcion;
      urlImag = snapshot.val().urlImag;
      escribirInputs();
    } else {
      limpiarInputs();
      mostrarMensaje("No se encontró el registro");
    }
  });
}

// Función para actualizar un automóvil
function actualizarAutomovil() {
  leerInputs();
  if (numSerie === "" || marca === "" || modelo === "" || descripcion === "") {
    mostrarMensaje("Favor de capturar toda la información.");
    return;
  }
  update(refS(db, 'Automoviles/' + numSerie), {
    numSerie: numSerie,
    marca: marca,
    modelo: modelo,
    descripcion: descripcion,
    urlImag: urlImag
  }).then(() => {
    mostrarMensaje("Se actualizó con éxito.");
    limpiarInputs();
    Listarproductos();
  }).catch((error) => {
    mostrarMensaje("Ocurrió un error: " + error);
  });
}

// Función para eliminar un automóvil
function eliminarAutomovil() {
  let numSerie = document.getElementById('txtNumSerie').value.trim();
  if (numSerie === "") {
    mostrarMensaje("No se ingresó un Código válido.");
    return;
  }
  const dbref = refS(db);
  get(child(dbref, 'Automoviles/' + numSerie)).then((snapshot) => {
    if (snapshot.exists()) {
      remove(refS(db, 'Automoviles/' + numSerie))
        .then(() => {
          mostrarMensaje("Producto eliminado con éxito.");
          limpiarInputs();
          Listarproductos();
        })
        .catch((error) => {
          mostrarMensaje("Ocurrió un error al eliminar el producto: " + error);
        });
    } else {
      limpiarInputs();
      mostrarMensaje("El producto con ID " + numSerie + " no existe.");
    }
  });
}

function escribirInputs() {
  document.getElementById('txtMarca').value = marca;
  document.getElementById('txtModelo').value = modelo;
  document.getElementById('txtDescripcion').value = descripcion;
  document.getElementById('txtUrl').value = urlImag;
}

function limpiarInputs() {
  document.getElementById('txtNumSerie').value = '';
  document.getElementById('txtMarca').value = '';
  document.getElementById('txtModelo').value = '';
  document.getElementById('txtDescripcion').value = '';
  document.getElementById('txtUrl').value = '';
}

// Añadir eventos a los botones
const btnBuscar = document.getElementById('btnBuscar');
btnBuscar.addEventListener('click', buscarAutomovile);
const btnBorrar = document.getElementById('btnBorrar');
btnBorrar.addEventListener('click', eliminarAutomovil);
const btnActualizar = document.getElementById('btnActualizar');
btnActualizar.addEventListener('click', actualizarAutomovil);
const btnAgregar = document.getElementById('btnAgregar');
btnAgregar.addEventListener('click', insertarProducto);

// Inicializar la lista de productos al cargar la página
Listarproductos();
